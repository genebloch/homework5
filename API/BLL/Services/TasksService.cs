﻿using BLL.Interfaces;
using DAL.Interfaces;
using DAL.Models;
using DTO;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BLL.Services
{
    public class TasksService : IService<TaskDTO>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly AutoMapper.IMapper _mapper;

        public TasksService(IUnitOfWork unitOfWork, AutoMapper.IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public bool Create(TaskDTO item)
        {
            var mapped = _mapper.Map<Task>(item);

            var results = new List<ValidationResult>();
            var context = new ValidationContext(mapped);

            if (!Validator.TryValidateObject(mapped, context, results, true)) return false;

            _unitOfWork.Tasks.Create(mapped);
            _unitOfWork.SaveChanges();
            return true;
        }

        public bool Delete(int id)
        {
            var status = _unitOfWork.Tasks.Delete(id);

            _unitOfWork.SaveChanges();

            return status;
        }

        public TaskDTO Get(int id)
        {
            var task = _unitOfWork.Tasks.Get(id);

            return _mapper.Map<TaskDTO>(task);
        }

        public IEnumerable<TaskDTO> GetAll()
        {
            var tasks = _unitOfWork.Tasks.GetAll();

            return _mapper.Map<IEnumerable<TaskDTO>>(tasks);
        }

        public bool Update(TaskDTO item)
        {
            var status = false;

            var mapped = _mapper.Map<Task>(item);

            var results = new List<ValidationResult>();
            var context = new ValidationContext(mapped);

            if (Validator.TryValidateObject(mapped, context, results, true))
            {
                status = _unitOfWork.Tasks.Update(mapped);

                _unitOfWork.SaveChanges();
            }

            return status;
        }
    }
}
