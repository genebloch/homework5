﻿using AutoMapper;
using BLL.AdditionalStructures;
using DAL.Interfaces;
using DTO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BLL.Services
{
    public class LinqService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public LinqService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public IDictionary<ProjectDTO, int> GetUserTasksInProject(int userId)
        {
            return _unitOfWork.Projects.GetAll().Where(p => p.AuthorId == userId).Select(p => new
            {
                project = p,
                tasksAmount = _unitOfWork.Tasks.GetAll().Count(task => task.ProjectId == p.Id)
            }).ToDictionary(x => _mapper.Map<ProjectDTO>(x.project), x => x.tasksAmount);
        }

        public IEnumerable<TaskDTO> GetUserTasksWithRestrictedNameLength(int userId)
        {
            var tasks = _unitOfWork.Tasks.GetAll().Where(t => t.PerformerId == userId)
                .Where(t => t.Name.Length < 45).ToList();

            return _mapper.Map<IEnumerable<TaskDTO>>(tasks);
        }

        public IEnumerable<TaskName> GetUserFinishedTasksIn2020(int userId)
        {
            return _unitOfWork.Tasks.GetAll().Where(t => t.PerformerId == userId)
                .Where(t => t.FinishedAt.Year == 2020)
                .Select(t => new TaskName() { Id = t.Id, Name = t.Name }).ToList();
        }

        public IEnumerable<UserDTO> GetUsersOlderThan10Years()
        {
            var users = _unitOfWork.Users.GetAll().Where(u => DateTime.Now.Year - u.Birthday.Year > 10)
                .OrderByDescending(u => u.RegisteredAt)
                .GroupBy(u => u.TeamId)
                .SelectMany(u => u).ToList();

            return _mapper.Map<IEnumerable<UserDTO>>(users);
        }

        public IEnumerable<UserWithTasks> GetSortedUsersWithTasks()
        {
            var output = new List<UserWithTasks>();

            var query = _unitOfWork.Users.GetAll().OrderBy(u => u.FirstName).Select(u => new
            {
                user = u,
                tasks = _unitOfWork.Tasks.GetAll().Where(t => t.PerformerId == u.Id).OrderByDescending(t => t.Name.Length).ToList()
            }).ToList();

            foreach (var obj in query)
            {
                output.Add(new UserWithTasks() { User = _mapper.Map<UserDTO>(obj.user), Tasks = _mapper.Map<IEnumerable<TaskDTO>>(obj.tasks) });
            }

            return output;
        }

        public UserInfo GetUserInfo(int userId)
        {
            var query = new
            {
                last = _unitOfWork.Projects.GetAll().Where(project => project.AuthorId == userId).OrderByDescending(p => p.CreatedAt).FirstOrDefault(),
                tasksAmount = -1, //Загальна кількість тасків під останнім проектом
                notFinishedTasks = _unitOfWork.Tasks.GetAll().Where(t => t.PerformerId == userId).Count(t => t.State == 3 || t.State == 4),
                theLongest = _unitOfWork.Tasks.GetAll().Where(t => t.PerformerId == userId).OrderByDescending(t => t.FinishedAt - t.CreatedAt).FirstOrDefault()
            };

            return new UserInfo()
            {
                LastProject = _mapper.Map<ProjectDTO>(query.last),
                NotFinishedTasks = query.notFinishedTasks,
                TasksAmount = query.tasksAmount,
                TheLongest = _mapper.Map<TaskDTO>(query.theLongest)
            };
        }

        public IEnumerable<ProjectInfo> GetProjectsInfo()
        {
            var output = new List<ProjectInfo>();

            var query = _unitOfWork.Projects.GetAll().Select(p => new
            {
                project = p,
                theLongestDescription = _unitOfWork.Tasks.GetAll().Where(t => t.ProjectId == p.Id).OrderByDescending(t => t.Description.Length).FirstOrDefault(),
                theShortestName = _unitOfWork.Tasks.GetAll().Where(t => t.ProjectId == p.Id).OrderBy(t => t.Name.Length).FirstOrDefault(),
                usersAmount = -1 //Загальна кількість користувачів в команді проекту, де або опис проекту >20 символів, або кількість тасків <3
            }).ToList();

            foreach (var obj in query)
            {
                output.Add(
                    new ProjectInfo() 
                    { 
                        Project = _mapper.Map<ProjectDTO>(obj.project), 
                        TheLongestDescription = _mapper.Map<TaskDTO>(obj.theLongestDescription),
                        TheShortestName = _mapper.Map<TaskDTO>(obj.theShortestName),
                        UsersAmount = obj.usersAmount
                    });
            }

            return output;
        }

        public IEnumerable<TaskDTO> GetUserNotFinishedTasks(int id)
        {
            var tasks = _unitOfWork.Tasks.GetAll().Where(t => t.PerformerId == id && t.State != 3).ToList();

            return _mapper.Map<IEnumerable<TaskDTO>>(tasks);
        }
    }
}
