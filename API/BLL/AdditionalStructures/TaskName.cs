﻿namespace BLL.AdditionalStructures
{
    public class TaskName
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
