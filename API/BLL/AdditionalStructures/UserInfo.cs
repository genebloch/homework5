﻿using DTO;

namespace BLL.AdditionalStructures
{
    public class UserInfo
    {
        public ProjectDTO LastProject { get; set; }
        public int TasksAmount { get; set; }
        public int NotFinishedTasks { get; set; }
        public TaskDTO TheLongest { get; set; }
    }
}
