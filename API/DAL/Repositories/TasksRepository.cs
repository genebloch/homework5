﻿using DAL.Context;
using DAL.Interfaces;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace DAL.Repositories
{
    public class TasksRepository : IRepository<Task>
    {
        private readonly DataContext _context;

        public TasksRepository() { }

        public TasksRepository(DataContext context)
        {
            _context = context;
        }

        public void Create(Task item) => _context.Tasks.Add(item);

        public bool Delete(int id)
        {
            var item = _context.Tasks.FirstOrDefault(t => t.Id == id);
            
            if (item == null) return false;

            _context.Tasks.Remove(item);
            return true;
        }

        public Task Get(int id) => _context.Tasks.FirstOrDefault(t => t.Id == id);
        public virtual IEnumerable<Task> GetAll() => _context.Tasks;

        public virtual bool Update(Task item)
        {
            var exists = _context.Tasks.Contains(item);

            if (!exists) return false;

            _context.Entry(item).State = EntityState.Modified;
            return true;
        }
    }
}
