﻿using DAL.Context;
using DAL.Interfaces;
using DAL.Models;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repositories
{
    public class UsersRepository : IRepository<User>
    {
        private readonly DataContext _context;

        public UsersRepository() { }

        public UsersRepository(DataContext context)
        {
            _context = context;
        }

        public virtual void Create(User item) => _context.Users.Add(item);

        public bool Delete(int id)
        {
            var item = _context.Users.FirstOrDefault(u => u.Id == id);

            if (item == null) return false;

            _context.Users.Remove(item);
            return true;
        }

        public User Get(int id) => _context.Users.FirstOrDefault(u => u.Id == id);
        public virtual IEnumerable<User> GetAll() => _context.Users;

        public virtual bool Update(User item)
        {
            var exists = _context.Users.Contains(item);

            if (!exists) return false;

            _context.Entry(item).State = EntityState.Modified;
            return true;
        }
    }
}
