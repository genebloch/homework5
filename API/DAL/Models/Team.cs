﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DAL.Models
{
    public class Team
    {
        public int Id { get; set; }
        [Required]
        [StringLength(20)]
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
