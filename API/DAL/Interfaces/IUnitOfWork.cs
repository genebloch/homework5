﻿using DAL.Repositories;
using System;

namespace DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        ProjectsRepository Projects { get; }
        TasksRepository Tasks { get; }
        TaskStatesRepository States { get; }
        TeamsRepository Teams { get; }
        UsersRepository Users { get; }

        int SaveChanges();
    }
}
